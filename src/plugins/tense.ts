import Vue from 'vue'

const tense = (word: string, count: number): string => {
  return count === 1 ? word : `${word}s`
}

Vue.prototype.$tense = tense
